# JeuxCartes
Package, sans aucune prétention, pour quelques commandes LaTeX d'insertion de cartes à jouer ou de mains de cartes à jouer.

![illustr](./github_pres_JeuxCartes.png?raw=true "illustr")

Licence des cartes de Poker : LGPL-2.1 license https://github.com/htdebeer/SVG-cards

Licence des cartes de Tarot : Public Domain https://freesvg.org/

Licence des cartes de Uno : Public Domain https://alexder.itch.io/uno-card-game-asset-pack
